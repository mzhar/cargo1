FROM debian:bookworm-slim

MAINTAINER paydala.kz

ENV JAVA_HOME=/opt/java/openjdk
COPY --from=eclipse-temurin:17 $JAVA_HOME $JAVA_HOME
ENV PATH="${JAVA_HOME}/bin:${PATH}"

RUN apt-get -y update
RUN apt-get install -y libfreetype6 fontconfig locales

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    sed -i -e 's/# ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/' /etc/locale.gen && \
    sed -i -e 's/# kk_KZ.UTF-8 UTF-8/kk_KZ.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

ARG JAR_FILE=target/*.jar
ARG APPLICATION_PROPERTIES=./src/main/resources/application.properties

COPY ${JAR_FILE} /app/app.jar
COPY ${APPLICATION_PROPERTIES} /app/application.properties

ENV SERVER_PORT=8080

RUN ln -sf /usr/share/zoneinfo/Asia/Almaty /etc/localtime

COPY jmx/jmxremote.access /jmx/jmxremote.access
COPY jmx/jmxremote.password /jmx/jmxremote.password
RUN chmod -R 0600 /jmx
RUN chown -R root:root /jmx/jmxremote.*

ENTRYPOINT ["java", \
    "-Dcom.sun.management.jmxremote", \
    "-Dcom.sun.management.jmxremote.port=9010", \
    "-Dcom.sun.management.jmxremote.local.only=false", \
    "-Dcom.sun.management.jmxremote.authenticate=false", \
    "-Dcom.sun.management.jmxremote.ssl=false", \
    "-Dcom.sun.management.jmxremote.authenticate=true", \
    "-Dcom.sun.management.jmxremote.password.file=/jmx/jmxremote.password", \
    "-Dcom.sun.management.jmxremote.access.file=/jmx/jmxremote.access", \
    "-Dcom.sun.management.jmxremote.local.only=false", \
    "-Dcom.sun.management.jmxremote.rmi.port=9011", \
    "-Djava.rmi.server.hostname=localhost", \
    "-jar","/app/app.jar", \
    "--spring.config.additional-location=file:///app/application.properties"]

EXPOSE 8080
EXPOSE 9010
EXPOSE 9011
