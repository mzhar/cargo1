var exampleModal = document.getElementById('exampleModal')
exampleModal.addEventListener('show.bs.modal', function (event) {
    // Button that triggered the modal
    var button = event.relatedTarget
    // Extract info from data-bs-* attributes
    var recipient = button.getAttribute('data-bs-whatever')
    var recipient2 = button.getAttribute('data-facultyName')
    var recipient3 = button.getAttribute('data-facultyNumber')
    // If necessary, you could initiate an AJAX request here
    // and then do the updating in a callback.
    //
    // Update the modal's content.
    var modalTitle = exampleModal.querySelector('.modal-title')
    var modalBodyInput = exampleModal.querySelector('.modal-body input')
    var modalBodyInput2 = exampleModal.querySelector('.modal-body .facultyName')
    var modalBodyInput3 = exampleModal.querySelector('.modal-body .facultyNumber')

    modalBodyInput.value = recipient
    modalBodyInput2.value = recipient2
    modalBodyInput3.value = recipient3
})
