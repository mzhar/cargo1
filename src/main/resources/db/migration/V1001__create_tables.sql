use `cargo`;

CREATE TABLE `users` (
                     `id` int NOT NULL AUTO_INCREMENT,
                     `created_dt` DATETIME DEFAULT NULL,
                     `email` varchar(128) DEFAULT NULL,
                     `enabled` boolean NOT NULL default true,
                     `name` varchar(128) DEFAULT NULL,
                     `password` varchar(128) DEFAULT NULL,
                     `patronymic` varchar(128) DEFAULT NULL,
                     `surname` varchar(128) DEFAULT NULL,
                     `username` varchar(128) DEFAULT NULL,
                                 PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ;

CREATE TABLE `user_role`
(
    `user_id` int(11) NOT NULL,
    `roles`   varchar(255) DEFAULT NULL,
    KEY `FKj345gk1bovqvfame88rcx7yyx` (`user_id`),
    CONSTRAINT `FKj345gk1bovqvfame88rcx7yyx` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `warehouse` (
                             `id` int NOT NULL AUTO_INCREMENT,
                             `name` varchar(128) DEFAULT NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `clients` (
                           `id` int NOT NULL AUTO_INCREMENT,
                           `code` varchar(255) NOT NULL,
                           `user_id` int DEFAULT NULL,
                           `warehouse_id` int DEFAULT NULL,
                           PRIMARY KEY (`id`),
                           KEY `FKtiuqdledq2lybrds2k3rfqrv4` (`user_id`),
                           KEY `FKdd7d4m4qqiyp1csgchm7exlh4` (`warehouse_id`),
                           CONSTRAINT `FKdd7d4m4qqiyp1csgchm7exlh4` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`),
                           CONSTRAINT `FKtiuqdledq2lybrds2k3rfqrv4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `products` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `created` DATETIME DEFAULT NULL,
                            `state` varchar(128) DEFAULT NULL,
                            `tracking_number` varchar(128) DEFAULT NULL,
                            `client_id` int DEFAULT NULL,
                            PRIMARY KEY (`id`),
                            KEY `FKns1dj6lgpfanpfcekxrevjh61` (`client_id`),
                            CONSTRAINT `FKns1dj6lgpfanpfcekxrevjh61` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `cargo`.`users` (`created_dt`, `email`, `enabled`, `name`, `username`, `password`, `patronymic`,
                                 `surname`)
VALUES ('2020-10-18 21:50:19.861300', 'test@mail.ru', '1', 'admin', 'admin',
        '$2a$10$6C.ONYKuPFGJdDk51o89ze1Ahrfi4shcwj/mySrt0Wwz0/f8lZfZa', 'admin', 'admin');
INSERT INTO `user_role` (roles, user_id)
VALUES ('ADMIN', 1);

INSERT INTO `cargo`.`users` (`created_dt`, `email`, `enabled`, `name`, `username`, `password`, `patronymic`,
                                 `surname`)
VALUES ('2020-10-18 21:50:19.861300', 'test@mail.ru', '1', 'user', 'user',
        '$2a$10$hiIQim5u8JDSo4gAHlCCSOPoVA8F9OccWOrLElGszxFDsAk2PisUy', 'user', 'user');
INSERT INTO `user_role` (roles, user_id)
VALUES ('USER', 2);

INSERT INTO `cargo`.`users` (`created_dt`, `email`, `enabled`, `name`, `username`, `password`, `patronymic`,
                             `surname`)
VALUES ('2021-02-25', 'test@mail.ru', '1', 'Caleb', 'anderson',
        '$2a$10$hiIQim5u8JDSo4gAHlCCSOPoVA8F9OccWOrLElGszxFDsAk2PisUy', '', 'Anderson');
INSERT INTO `user_role` (roles, user_id)
VALUES ('USER', 3);

INSERT INTO cargo.warehouse (name) VALUES ('Алматы');
INSERT INTO cargo.warehouse (name) VALUES ('Астана');

INSERT INTO cargo.clients (warehouse_id, user_id, code) VALUES (1, 1, 'ALM1001');
INSERT INTO cargo.clients (warehouse_id, user_id, code) VALUES (1, 2, 'ALM1002');
INSERT INTO cargo.clients (warehouse_id, user_id, code) VALUES (2, 3, 'AST1003');

INSERT INTO cargo.products (created, state, tracking_number, client_id) VALUES ('2020-10-18 21:50:19.861300', 'IN_WAREHOUSE', '123456789', 1);
INSERT INTO cargo.products (created, state, tracking_number, client_id) VALUES ('2021-10-18 21:50:19.861300', 'IN_WAREHOUSE', '123456788', 2);





