package kz.cargo.delivery.cargo.dto;

import com.mysema.query.annotations.QueryEntity;
import kz.cargo.delivery.cargo.model.User;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@ToString
@QueryEntity
@Builder(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserDTO {

    private int id;
    private String name;
    private String surname;
    private String patronymic;
    private LocalDateTime createdDate;
    private String email;
    private String code;

    public static UserDTO from(User user) {
        return builder()
                .id(user.getId())
                .name(user.getName())
                .surname(user.getSurname())
                .patronymic(user.getPatronymic())
                .createdDate(user.getCreatedDt())
                .email(user.getEmail())
                .build();
    }
}
