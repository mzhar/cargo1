package kz.cargo.delivery.cargo.dto;

import com.mysema.query.annotations.QueryEntity;
import kz.cargo.delivery.cargo.model.Role;
import lombok.*;

@Data
@ToString
@QueryEntity
@Builder(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserRoleDTO {
    private int id;
    private String role;

    public static UserRoleDTO from(int id, String role) {
        return builder()
                .id(id)
                .role(role)
                .build();
    }
}
