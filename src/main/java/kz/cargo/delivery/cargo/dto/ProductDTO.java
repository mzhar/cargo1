package kz.cargo.delivery.cargo.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.mysema.query.annotations.QueryEntity;
import kz.cargo.delivery.cargo.model.Client;
import kz.cargo.delivery.cargo.model.Product;
import kz.cargo.delivery.cargo.model.ProductState;
import kz.cargo.delivery.cargo.model.User;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@ToString
@QueryEntity
@Builder(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductDTO {
    private Integer id;
    private String trackingNumber;
    private ProductState state;
    private String created;
    private Client client;

    public static ProductDTO from(Product product) {
        return builder()
                .id(product.getId())
                .trackingNumber(product.getTrackingNumber())
                .state(product.getState())
                .created(product.getCreated().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                .client(product.getClient())
                .build();
    }
}
