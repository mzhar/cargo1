package kz.cargo.delivery.cargo.model;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    USER("Пользователь"), ADMIN("Админ");

    Role(String role) {
        this.role = role;
    }

    String role;

    @Override
    public String getAuthority() {
        return name();
    }

    public String getRole() {
        return role;
    }
}
