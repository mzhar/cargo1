//package kz.cargo.delivery.cargo.model;
//
//import com.fasterxml.jackson.annotation.JsonBackReference;
//import lombok.*;
//import javax.persistence.*;
//
//@Entity
//@Builder
//@NoArgsConstructor
//@Table(name = "user_roles")
//@AllArgsConstructor(access = AccessLevel.PACKAGE)
//public class UserRole {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
//    private Long id;
//
//    private String role;
//
//    @JsonBackReference
//    @ManyToOne
//    @JoinColumn(name = "user_id")
//    private User user;
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getRole() {
//        return role;
//    }
//
//    public void setRole(String role) {
//        this.role = role;
//    }
//
//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }
//}
