package kz.cargo.delivery.cargo.model;

public enum ProductState {
    IN_WAREHOUSE("Ожидает на складе"), READY("Готов к выдаче"), ISSUED("Выдан");

    ProductState(String state) {
        this.state = state;
    }

    String state;

    public String getState() {
        return state;
    }

    public static ProductState from(String state) {
        for (ProductState p : values()) {
            if (p.state.equals(state)) {
                return p;
            }
        }
        return null;
    }
}
