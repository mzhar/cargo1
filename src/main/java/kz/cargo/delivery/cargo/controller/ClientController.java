package kz.cargo.delivery.cargo.controller;

import kz.cargo.delivery.cargo.model.Client;
import kz.cargo.delivery.cargo.model.Product;
import kz.cargo.delivery.cargo.model.Role;
import kz.cargo.delivery.cargo.model.User;
import kz.cargo.delivery.cargo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Controller
public class ClientController {

    @Autowired
    private UserService userService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private ProductService productService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private WarehouseService warehouseService;

    @GetMapping("/users")
    public String getUsers(Model model,
                           @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
                           HttpServletRequest uriBuilder,
                           Principal principal
    ) {
        if (userService.isAuthUser(principal)) {
            model.addAttribute("currentUser", userService.getByUsername(principal.getName()));
        }

        if (userService.isAdmin(principal)) {
            model.addAttribute("admin", true);
        }

        String uri = uriBuilder.getRequestURI();
        model.addAttribute("url", uri);
        model.addAttribute("page", clientService.getClients(pageable));
        model.addAttribute("roles", Arrays.stream(Role.values()).collect(toList()));
        model.addAttribute("role", userRoleService.getUserRoles());
        model.addAttribute("warehouses", warehouseService.getWarehouses().stream()
                .collect(toList()));
        return "users";
    }

    @GetMapping("/client/{id}/delete")
    public String deleteClient(
            @PathVariable("id") int clientId,
            RedirectAttributes attributes,
            Principal principal
    ) {
        Client client = clientService.getClientById(clientId).get();
        int currentUserId = userService.getByUsername(principal.getName()).getId();
        int userIdDeleted = client.getUser().getId();

        if (currentUserId == userIdDeleted) {
            attributes.addFlashAttribute("errors", "Нет доступа!");
            return "redirect:/users";
        }

        try {
            if (clientService.getClientById(clientId).isPresent()) {
                List<Product> productList = clientService.getClientById(clientId).get().getProducts();
                productService.deleteAll(productList);
            }
            clientService.deleteById(clientId);
            attributes.addFlashAttribute("success", "Успешно");
        } catch (Exception e) {
            attributes.addFlashAttribute("errors", e.getMessage());
            return "redirect:/users";
        }

        return "redirect:/users";
    }


    @GetMapping("/client/{id}/{action}")
    public String deactivateClient(
            @PathVariable("id") int clientId,
            @PathVariable("action") String action,
            RedirectAttributes attributes,
            Principal principal
    ) {
        Client client = clientService.getClientById(clientId).get();
        int currentUserId = userService.getByUsername(principal.getName()).getId();
        int userIdDeleted = client.getUser().getId();

        if (currentUserId == userIdDeleted) {
            attributes.addFlashAttribute("errors", "Нет доступа!");
            return "redirect:/users";
        }

        try {
            if ("deactivate".equals(action)) {
                clientService.setEnabled(clientId, false);
            }
            if ("activate".equals(action)) {
                clientService.setEnabled(clientId, true);
            }

            attributes.addFlashAttribute("success", "Успешно");
        } catch (Exception e) {
            attributes.addFlashAttribute("errors", e.getMessage());
            return "redirect:/users";
        }

        return "redirect:/users";
    }

    @PostMapping("/client/add")
    public String addStudent(
            @Valid @ModelAttribute User user,
            BindingResult validationResult,
            RedirectAttributes attributes,
            @RequestParam String warehouseId
    ) {

        attributes.addFlashAttribute("form", user);

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/users";
        }

        clientService.saveClient(user, warehouseId, user.getRoles());
        attributes.addFlashAttribute("success", "Успешно");

        return "redirect:/users";

    }

    @PostMapping("/client/update")
    public String updateStudent(
            @Valid @ModelAttribute User user,
            BindingResult validationResult,
            RedirectAttributes attributes,
            @RequestParam String warehouseId
    ) {

        attributes.addFlashAttribute("form", user);

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors()
                    .get(0).getDefaultMessage());
            return "redirect:/users";
        }

        userService.saveUser(user, user.getRoles());

        return "redirect:/users";

    }

    @PostMapping("/filtere-client")
    public String filteredStudent(
            @RequestParam String criteria,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
            Model model,
            Principal principal
    ) {

        if (userService.isAuthUser(principal)) {
            model.addAttribute("currentUser", userService.getByUsername(principal.getName()));
        }

        if (userService.isAdmin(principal)) {
            model.addAttribute("admin", true);
        }

        model.addAttribute("clients", userService.filter(criteria));
        model.addAttribute("roles", Arrays.stream(Role.values()).collect(toList()));
        model.addAttribute("role", userRoleService.getUserRoles());
        model.addAttribute("warehouses", warehouseService.getWarehouses().stream()
                .collect(toList()));
        //model.addAttribute("page", clientService.getClients(pageable));

        return "filtered-client";
    }
}
