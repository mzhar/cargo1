package kz.cargo.delivery.cargo.controller;

import kz.cargo.delivery.cargo.model.Client;
import kz.cargo.delivery.cargo.model.Role;
import kz.cargo.delivery.cargo.model.User;
import kz.cargo.delivery.cargo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Arrays;

import static java.util.stream.Collectors.toList;

@Controller
public class WarehousesController {

    @Autowired
    private UserService userService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private ProductService productService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private WarehouseService warehouseService;

    @GetMapping("/warehouses")
    public String getWarehouses(Model model,
                              @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
                              HttpServletRequest uriBuilder,
                              Principal principal
    ) {
        User user = userService.getByUsername(principal.getName());
        Client client = clientService.getClientByUserId(user.getId()).get();

        if (userService.isAuthUser(principal)) {
            model.addAttribute("currentUser", user);
        }

        if (userService.isAdmin(principal)) {
            model.addAttribute("admin", true);
        }

        String uri = uriBuilder.getRequestURI();
        model.addAttribute("url", uri);
        model.addAttribute("page", clientService.getClients(pageable));
        model.addAttribute("roles", Arrays.stream(Role.values()).collect(toList()));
        model.addAttribute("role", userRoleService.getUserRoles());
        model.addAttribute("warehouses", warehouseService.getWarehouses().stream()
                .collect(toList()));
        model.addAttribute("clientId", client.getId());

        return "warehouses";
    }
}
