package kz.cargo.delivery.cargo.controller;

import kz.cargo.delivery.cargo.model.*;
import kz.cargo.delivery.cargo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Controller
public class MainController {

    @Autowired
    private UserService userService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private ProductService productService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private WarehouseService warehouseService;

    @GetMapping("/")
    public String indexPage(Model model, Principal principal) {

        if (userService.isAuthUser(principal)) {
            model.addAttribute("currentUser", userService.getByUsername(principal.getName()));
        }

        if (userService.isAdmin(principal)) {
            model.addAttribute("admin", true);
        }

        return "index";
    }

    @GetMapping("/login")
    public String loginPage(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }

    @GetMapping("/register")
    public String getRegistationPage(Model model) {
        model.addAttribute("warehouses", warehouseService.getWarehouses().stream()
                .collect(toList()));
        return "reg";
    }

    @PostMapping("/register")
    public String registerPage(@Valid User user,
                               BindingResult validationResult,
                               RedirectAttributes attributes,
                               @RequestParam String warehouseId) {
        attributes.addFlashAttribute("form", user);

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/register";
        }

        clientService.saveClient(user, warehouseId, Collections.singleton(Role.USER));
        return "redirect:/login";
    }

    @GetMapping("/forbidden")
    public String forbidden(Principal principal, Model model) {
        if (userService.isAuthUser(principal))
            model.addAttribute("currentUser", userService.getByUsername(principal.getName()));
        model.addAttribute("userPrincipal", principal.getName());
        return "forbidden";
    }

}
