package kz.cargo.delivery.cargo.controller;

import kz.cargo.delivery.cargo.model.*;
import kz.cargo.delivery.cargo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;

import static java.util.stream.Collectors.toList;

@Controller
public class ProductController {

    @Autowired
    private UserService userService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private ProductService productService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private WarehouseService warehouseService;

    @GetMapping("/products")
    public String getProducts(Model model,
                           @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
                           HttpServletRequest uriBuilder,
                           Principal principal
    ) {
        User user = userService.getByUsername(principal.getName());
        Client client = clientService.getClientByUserId(user.getId()).get();

        if (userService.isAuthUser(principal)) {
            model.addAttribute("currentUser", user);
        }

        if (userService.isAdmin(principal)) {
            model.addAttribute("admin", true);
        }

        String uri = uriBuilder.getRequestURI();
        model.addAttribute("url", uri);
        model.addAttribute("page", clientService.getClients(pageable));
        model.addAttribute("roles", Arrays.stream(Role.values()).collect(toList()));
        model.addAttribute("role", userRoleService.getUserRoles());
        model.addAttribute("warehouses", warehouseService.getWarehouses().stream()
                .collect(toList()));
        model.addAttribute("products", productService.getProductsByClientId(client.getId()));
        model.addAttribute("clientId", client.getId());

        return "products";
    }

    @PostMapping("/product/add")
    public String addProduct(
            @Valid @ModelAttribute Product product,
            BindingResult validationResult,
            RedirectAttributes attributes,
            @RequestParam String clientId
    ) {
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/users";
        }

        productService.saveProduct(Integer.parseInt(clientId), product.getTrackingNumber());
        attributes.addFlashAttribute("success", "Успешно");

        return "redirect:/products";
    }

    @PostMapping("/product/update")
    public String updateProduct(
            @Valid @ModelAttribute Product product,
            BindingResult validationResult,
            RedirectAttributes attributes,
            @RequestParam String productId
    ) {

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/products";
        }

        productService.updateProduct(product.getTrackingNumber(), Integer.parseInt(productId));
        attributes.addFlashAttribute("success", "Успешно");

        return "redirect:/products";

    }

    @GetMapping("/product/{id}/delete")
    public String deleteProduct(
            @PathVariable("id") int productId,
            RedirectAttributes attributes
    ) {

        try {
            productService.deleteProduct(productId);
            attributes.addFlashAttribute("success", "Успешно");
        } catch (Exception e) {
            attributes.addFlashAttribute("errors", e.getMessage());
            return "redirect:/users";
        }

        return "redirect:/products";
    }

    @GetMapping("/product/{id}/{status}")
    public String changeStatusProduct(
            @PathVariable("id") int productId,
            @PathVariable("status") String state,
            RedirectAttributes attributes,
            Principal principal,
            Model model
    ) {
        if (userService.isAdmin(principal)) {
            model.addAttribute("admin", true);
        }

        try {
            productService.changeStatus(productId, state);
            attributes.addFlashAttribute("success", "Успешно");
        } catch (Exception e) {
            attributes.addFlashAttribute("errors", e.getMessage());
            return "redirect:/users";
        }

        return "redirect:/products";
    }

    @PostMapping("/filtere-product")
    public String filteredProduct(
            @RequestParam String criteria,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable,
            HttpServletRequest uriBuilder,
            Model model,
            Principal principal
    ) {
        User user = userService.getByUsername(principal.getName());
        Client client = clientService.getClientByUserId(user.getId()).get();

        if (userService.isAuthUser(principal)) {
            model.addAttribute("currentUser", userService.getByUsername(principal.getName()));
        }

        if (userService.isAdmin(principal)) {
            model.addAttribute("admin", true);
        }

        String uri = uriBuilder.getRequestURI();
        model.addAttribute("url", uri);
        model.addAttribute("page", clientService.getClients(pageable));
        model.addAttribute("products", productService.filter(criteria));
        model.addAttribute("roles", Arrays.stream(Role.values()).collect(toList()));
        model.addAttribute("role", userRoleService.getUserRoles());
        model.addAttribute("warehouses", warehouseService.getWarehouses().stream()
                .collect(toList()));
        //model.addAttribute("page", clientService.getClients(pageable));
        model.addAttribute("clientId", client.getId());

        return "filtered-product";
    }
}
