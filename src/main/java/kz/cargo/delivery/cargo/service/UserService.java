package kz.cargo.delivery.cargo.service;

import com.mysema.query.jpa.impl.JPAQuery;
import kz.cargo.delivery.cargo.dto.UserDTO;
import kz.cargo.delivery.cargo.exception.CustomerAlreadyRegisteredException;
import kz.cargo.delivery.cargo.exception.CustomerNotFoundException;
import kz.cargo.delivery.cargo.model.Client;
import kz.cargo.delivery.cargo.model.Role;
import kz.cargo.delivery.cargo.model.User;
import kz.cargo.delivery.cargo.repo.UserRepo;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;
import  kz.cargo.delivery.cargo.model.QClient;

import javax.persistence.EntityManager;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.*;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UserService {

    @Autowired
    EntityManager entityManager;

    private final PasswordEncoder encoder;

    private final UserRepo userRepo;

    public Page<UserDTO> getUsers(Pageable pageable) {
        return userRepo.findAll(pageable).map(UserDTO::from);
    }

    public void saveUser(User u, Set<Role> role) {
        User user = User.builder()
                .id(u.getId())
                .name(u.getName())
                .surname(u.getSurname())
                .patronymic(u.getPatronymic())
                .createdDt(LocalDateTime.now())
                .email(u.getEmail())
                .username(u.getUsername())
                .password(encoder.encode(u.getPassword()))
                .roles(role)
                .build();

        userRepo.save(user);
    }

    public Optional<User> getUserById(int id) {
        return userRepo.findById(id);
    }

    public void deleteUser(int studentId) {
        userRepo.deleteById(studentId);
    }

    public User register(User user) {

        if (userRepo.existsByUsername(user.getUsername())) {
            throw new CustomerAlreadyRegisteredException();
        }

        User u = User.builder()
                .username(user.getUsername())
                .password(encoder.encode(user.getPassword()))
                .roles(Collections.singleton(Role.USER))
                .build();

        return userRepo.save(u);
    }

    public User getByUsername(String uname) {
        return userRepo.findByUsername(uname)
                .orElseThrow(CustomerNotFoundException::new);
    }

    public boolean isAuthUser(Principal principal) {
        try {
            principal.getName();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<Client> filter(String criteria) {
        QClient client = QClient.client;

        JPAQuery query = new JPAQuery(entityManager);

        if (!criteria.equals("")) {
            return query.from(client)
                    .where(client.user.surname.like("%" + criteria + "%")
                            .or(client.user.name.like("%" + criteria + "%"))
                            .or(client.user.patronymic.like("%" + criteria + "%"))
                            .or(client.user.email.like("%" + criteria + "%")
                                    .or(client.user.username.like("%" + criteria + "%"))))
                    .list(client);
        }

      return new ArrayList<>();
    }

    public boolean isAdmin(Principal principal) {
        String userRole = getByUsername(principal.getName()).getRoles().iterator().next().name();
        if (userRole.equals(Role.ADMIN.name())) {
            return true;
        }

        return false;
    }


}