package kz.cargo.delivery.cargo.service;

import com.mysema.query.jpa.impl.JPAQuery;
import kz.cargo.delivery.cargo.dto.ProductDTO;
import kz.cargo.delivery.cargo.model.*;
import kz.cargo.delivery.cargo.repo.ClientRepo;
import kz.cargo.delivery.cargo.repo.ProductRepo;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductService {

    private final ProductRepo productRepo;
    private final ClientRepo clientRepo;
    private final EntityManager entityManager;

    public List<Product> getProducts() {
        return productRepo.findAll();
    }

    public List<ProductDTO> getProductsByClientId(Integer i) {
        return productRepo.findAllByClientId(i).stream()
                .map(e -> ProductDTO.from(e))
                .collect(Collectors.toList());
    }

    public void deleteProduct(int id) {
        productRepo.deleteById(id);
    }

    public Optional<Product> changeStatus(int id, String state) {
        Optional<Product> result = Optional.empty();
        Optional<Product> product = productRepo.findById(id);
        if (product.isPresent()) {
            for (ProductState productState : ProductState.values()) {
                if (productState.valueOf(state) == productState) {
                    product.get().setState(productState);
                    productRepo.save(product.get());
                    result = product;
                }
            }

        }
        return result;
    }

    public void deleteAll(List<Product> products) {
        productRepo.deleteAll(products);
    }

    public void saveProduct(int clientId, String trackingNumber) {
        Client client = clientRepo.findById(clientId).get();

        Product product = Product.builder()
                .client(client)
                .created(LocalDateTime.now())
                .trackingNumber(trackingNumber)
                .state(ProductState.IN_WAREHOUSE)
                .build();

        productRepo.save(product);
    }

    public void updateProduct(String trackingNumber, int productId) {
        Optional<Product> product = productRepo.findById(productId);
        if (product.isPresent()) {
           Product updatedProduct = product.get();
           updatedProduct.setTrackingNumber(trackingNumber);
           productRepo.save(updatedProduct);
        }
    }

    public List<Product> filter(String criteria) {
        QProduct product = QProduct.product;

        JPAQuery query = new JPAQuery(entityManager);

        if (!criteria.equals("")) {
            return query.from(product)
                    .where(product.trackingNumber.like("%" + criteria + "%"))
                    .list(product);
        }

        return new ArrayList<>();
    }
}

