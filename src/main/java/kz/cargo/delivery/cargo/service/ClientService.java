package kz.cargo.delivery.cargo.service;

import kz.cargo.delivery.cargo.model.Client;
import kz.cargo.delivery.cargo.model.Role;
import kz.cargo.delivery.cargo.model.User;
//import kz.cargo.delivery.cargo.model.UserRole;
//import kz.cargo.delivery.cargo.model.Warehouse;
import kz.cargo.delivery.cargo.model.Warehouse;
import kz.cargo.delivery.cargo.repo.ClientRepo;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ClientService {
    private static final Map<Character, String> CYRILLIC_TO_LATIN_MAP = new HashMap<Character, String>() {{
        put('А', "A");
        put('а', "a");
        put('Б', "B");
        put('б', "b");
        put('В', "V");
        put('в', "v");
        put('Г', "G");
        put('г', "g");
        put('Д', "D");
        put('д', "d");
        put('Е', "E");
        put('е', "e");
        put('Ё', "E");
        put('ё', "e");
        put('Ж', "ZH");
        put('ж', "zh");
        put('З', "Z");
        put('з', "z");
        put('И', "I");
        put('и', "i");
        put('Й', "I");
        put('й', "i");
        put('К', "K");
        put('к', "k");
        put('Л', "L");
        put('л', "l");
        put('М', "M");
        put('м', "m");
        put('Н', "N");
        put('н', "n");
        put('Ң', "N");
        put('ң', "n");
        put('О', "O");
        put('о', "o");
        put('Ө', "O");
        put('ө', "o");
        put('П', "P");
        put('п', "p");
        put('Р', "R");
        put('р', "r");
        put('С', "S");
        put('с', "s");
        put('Т', "T");
        put('т', "t");
        put('У', "U");
        put('у', "u");
        put('Ү', "U");
        put('ү', "u");
        put('Ф', "F");
        put('ф', "f");
        put('Х', "KH");
        put('х', "kh");
        put('Ц', "TS");
        put('ц', "ts");
        put('Ч', "CH");
        put('ч', "ch");
        put('Ш', "SH");
        put('ш', "sh");
        put('Щ', "SHCH");
        put('щ', "shch");
        put('Ь', "");
        put('ь', "");
        put('Ы', "Y");
        put('ы', "y");
        put('Ъ', "");
        put('ъ', "");
        put('Э', "E");
        put('э', "e");
        put('Ю', "YU");
        put('ю', "yu");
        put('Я', "YA");
        put('я', "ya");
    }};

    private final ClientRepo clientRepo;

    private final PasswordEncoder encoder;

    @Autowired
    private final WarehouseService warehouseService;

    //private final PasswordEncoder encoder;

    public Page<Client> getClients(Pageable pageable) {
        return clientRepo.findAll(pageable);
    }

    public Optional<Client> getClientById(int id) {
        return clientRepo.findById(id);
    }

    public Optional<Client> getClientByUserId(int id) {
        return clientRepo.findByUserId(id);
    }
    public void deleteById(int client) {
        clientRepo.deleteById(client);
    }

    public void setEnabled(int id, boolean isEnable) {
        Optional<Client> clientOptional = clientRepo.findById(id);
        if (clientOptional.isPresent()) {
            Client client = clientOptional.get();
            User user = client.getUser();
            user.setEnabled(isEnable);

            clientRepo.save(client);
        }
    }

    public void saveClient(User user, String warehouseId, Set<Role> role) {
        Warehouse warehouse = warehouseService.getWarehouseById(Integer.parseInt(warehouseId)).get();

        User userBuilder = User.builder()
                .name(user.getName())
                .surname(user.getSurname())
                .patronymic(user.getPatronymic())
                .email(user.getEmail())
                .username(user.getUsername())
                .password(encoder.encode(user.getPassword()))
                .createdDt(LocalDateTime.now())
                .roles(role)
                .build();

       String code = getCityCode(warehouse.getName().toUpperCase().substring(0,3));
       Optional<Client> lastClient = clientRepo.findById(clientRepo.findAll().size());
       int numberCode = Integer.parseInt(
               lastClient.get().getCode().substring(3)) + 1;

        Client newClient = Client.builder()
                .user(userBuilder)
                .warehouse(warehouse)
                .code(code+numberCode)
                .build();

        clientRepo.save(newClient);
    }

    private String convertCyrillicToLatin(String value) {
        if (value == null || value.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < value.length(); ++i) {
            if (CYRILLIC_TO_LATIN_MAP.containsKey(value.charAt(i))) {
                sb.append(CYRILLIC_TO_LATIN_MAP.get(value.charAt(i)));
            } else {
                sb.append(value.charAt(i));
            }
        }
        return sb.toString();
    }

    private String getCityCode(String value) {
        final String fullSenderName = convertCyrillicToLatin(
                Optional.ofNullable(value)
                        .orElse("")
                        .replace(" ", "").trim());
        return fullSenderName;
    }
}
