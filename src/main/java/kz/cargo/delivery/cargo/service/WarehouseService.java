package kz.cargo.delivery.cargo.service;

import kz.cargo.delivery.cargo.model.Warehouse;
import kz.cargo.delivery.cargo.repo.WarehouseRepo;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class WarehouseService {
    private final WarehouseRepo warehouseRepo;

    public List<Warehouse> getWarehouses() {
        return warehouseRepo.findAll();
    }

    public Optional<Warehouse> getWarehouseById(int id) {
        return warehouseRepo.findById(id);
    }

//    public Optional<Warehouse> getByClientId(int id) {
//        return warehouseRepo.getByClientId(id);
//    }

    public void deleteById(int id) {
        warehouseRepo.deleteById(id);
    }
}
