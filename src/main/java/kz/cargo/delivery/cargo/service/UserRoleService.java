package kz.cargo.delivery.cargo.service;

import kz.cargo.delivery.cargo.dto.UserRoleDTO;
import kz.cargo.delivery.cargo.model.Client;
import kz.cargo.delivery.cargo.repo.ClientRepo;
import kz.cargo.delivery.cargo.repo.UserRoleRepo;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UserRoleService {

    private final UserRoleRepo userRoleRepo;

    public List<UserRoleDTO> getUserRoles() {
        return userRoleRepo.getUserRoles();
    }
}
