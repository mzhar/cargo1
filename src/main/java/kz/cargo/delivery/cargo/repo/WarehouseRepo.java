package kz.cargo.delivery.cargo.repo;

import kz.cargo.delivery.cargo.model.User;
import kz.cargo.delivery.cargo.model.Warehouse;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface WarehouseRepo extends JpaRepository<Warehouse, Integer> {
    //Optional<Warehouse> getByClientId(int id);
}
