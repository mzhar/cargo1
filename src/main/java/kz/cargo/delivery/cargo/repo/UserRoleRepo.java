package kz.cargo.delivery.cargo.repo;

import kz.cargo.delivery.cargo.dto.UserRoleDTO;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserRoleRepo {
    private static final Logger logger = Logger.getLogger(UserRoleRepo.class);
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public List<UserRoleDTO> getUserRoles() {
        List<UserRoleDTO> params = new ArrayList<>();

        try {
            String sqlQuery = "select * from user_role;";

            Query query = entityManager.createNativeQuery(sqlQuery);

            List<Object[]> results = query.getResultList();

            for (Object[] result : results) {
                Integer key = (Integer)result[0];
                String value = (String) result[1];
                params.add(UserRoleDTO.from(key, value));
            }

        } catch (Exception e) {
            logger.error("Get error", e);
            throw new RuntimeException();
        }

        return params;
    }
}
