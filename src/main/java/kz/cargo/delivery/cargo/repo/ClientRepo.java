package kz.cargo.delivery.cargo.repo;

import kz.cargo.delivery.cargo.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientRepo extends JpaRepository<Client, Integer> {
    Optional<Client> findByUserId(int id);
}
